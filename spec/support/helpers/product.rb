module ProductHelper
  def visit_product_page
    visit('/products/trudeau-graviti-electric-salt-pepper-mill-gunmetal/')
  end

  def add_product_to_cart
    visit_product_page
    click_button "Add to Cart"
  end
end
