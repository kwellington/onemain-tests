module SearchResultsHelper
  def visit_search_results_for(query)
    visit "/search/results.html?words=fry%20pan"
    remove_marketing_overlay
  end

  def open_first_quicklook
    remove_marketing_overlay
    page.find(".quicklook-link", match: :first).click
  end
end
