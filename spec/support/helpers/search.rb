module SearchHelper
  def search_for(query)
    visit "/"
    remove_marketing_overlay
    fill_in 'search-field', with: query
    click_link 'btnSearch'
  end
end
