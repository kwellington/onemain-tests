module RemoveMarketingOverlayHelper
  def remove_marketing_overlay
    sleep 2
    page.execute_script("document.querySelectorAll('iframe').forEach((elem) => elem.parentNode.removeChild(elem));")
  end
end
