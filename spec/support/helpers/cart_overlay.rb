module CartOverlayHelper
  def overlay_selector
    '#racOverlay'
  end

  def begin_checkout
    remove_marketing_overlay
    click_link("anchor-btn-checkout")
  end
end
