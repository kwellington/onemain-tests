RSpec.describe "Search Result", type: :request do
  subject { page }
  
  before do
    visit_search_results_for "fry_pan"
  end

  it 'should have quick look link' do
    is_expected.to have_link("Quicklook")
  end

  it 'quick look link should open' do
    open_first_quicklook
    is_expected.to have_css("#quicklookOverlay")
  end

  describe "when compared with quick look" do
    before do

      within(".product-cell", match: :first) do
        @search_title = page.find(".product-name").text
        @search_price = page.find(".price-sale-amount").text
      end

      open_first_quicklook

      within("#quicklookOverlay") do
        @ql_title = page.find("h1").text
        @ql_price = page.find(".price-special").text
      end
    end

    it 'should match product title' do
      expect(@search_title).to eq(@ql_title)
    end

    it 'should match product price' do
      expect(@search_price).to eq(@ql_price)
    end
  end
end
