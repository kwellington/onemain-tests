RSpec.describe "Search", type: :request do
  let(:query) { "Fry Pan" }
  
  it 'query should return results' do
    search_for query
    expect(page).to have_content(query)
  end
end
