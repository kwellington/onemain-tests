RSpec.describe "Cart", type: :request do
  it 'should show added products' do
    # In the future, this test could dynamically check whether the product title matches the cart (instead of using preselected values like I did below)
    add_product_to_cart
    visit_cart_page
    expect(page).to have_content("Trudeau Graviti Electric Salt & Pepper Mill Set, Gunmetal")
  end
end
