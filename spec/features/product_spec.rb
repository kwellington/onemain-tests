RSpec.describe "Product", type: :request do
  subject { page }

  before do
    visit_product_page
  end

  it 'should be viewable' do
    is_expected.to have_content('Trudeau')
  end

  it 'should have a button to add to cart' do
    is_expected.to have_button('Add to Cart')
  end
end
