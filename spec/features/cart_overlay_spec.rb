RSpec.describe "Cart Overlay", type: :request do
  before do
    add_product_to_cart
  end

  it 'should display when product added to cart' do
    page.find(overlay_selector).should have_content('Trudeau Graviti Electric Salt & Pepper Mill Set, Gunmetal')
  end

  it 'should have a checkout button' do
    page.find(overlay_selector).should have_content('Trudeau Graviti Electric Salt & Pepper Mill Set, Gunmetal')
  end

  it 'should redirect to checkout when checkout button clicked' do
    begin_checkout
    expect(current_path).to eq "/shoppingcart/"
  end
end
