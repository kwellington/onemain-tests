# Installing the Environment

You can download the required gems with bundler.

```
bundle
```

You may also need the geckodriver if you don't have it yet. You can download it from GitHub [here](https://github.com/mozilla/geckodriver/releases), and then you just need to put it in your PATH (on macOS, you can put it in `/usr/local/bin`).
